package com.example.andreafranceschini.farmacal;

import android.provider.BaseColumns;

/**
 * Created by andreafranceschini on 06/04/17.
 */

    public final class DataContract {
        // To prevent someone from accidentally instantiating the contract class,
        // make the constructor private.
        private DataContract() {}

        /* Inner class that defines the table contents */
        public static class ThreadEntry implements BaseColumns {
            public static final String TH_TABLE_NAME = "THREAD";
            public static final String TH_COL_NAME = "THNAME";
            public static final String TH_COL_ALERT = "THALERT";
            public static final String TH_COL_DATE = "THDATE";
        }

        public static class PharmEntry implements BaseColumns {
            public static final String PH_TABLE_NAME = "PHARM";
            public static final String PH_COL_NAME = "PHNAME";
            public static final String PH_COL_TIMELAPSE = "PHTIMELAPSE";
            public static final String PH_COL_ACTIVE = "PHACTIVE";
            public static final String PH_COL_THID = "THREAD_ID";
        }

        public static class PharmRowEntry implements BaseColumns {
            public static final String PR_TABLE_NAME = "PHARMROW";
            //public static final String PR_COL_NAME = "PRNAME";
            public static final String PR_COL_DATEDONE = "PRDATEDONE";
            public static final String PR_COL_PHID = "PHARM_ID";
        }


        public static final String SQL_CREATE_THREADS =
                "CREATE TABLE " + ThreadEntry.TH_TABLE_NAME + " (" +
                        ThreadEntry._ID + " INTEGER PRIMARY KEY," +
                        ThreadEntry.TH_COL_NAME + " TEXT," +
                        ThreadEntry.TH_COL_ALERT + " INTEGER," +
                        ThreadEntry.TH_COL_DATE + " INTEGER)";

        public static final String SQL_DELETE_THREADS =
                "DROP TABLE IF EXISTS " + ThreadEntry.TH_TABLE_NAME;

        public static final String SQL_CREATE_PHARMS =
                "CREATE TABLE " + PharmEntry.PH_TABLE_NAME + " (" +
                        PharmEntry._ID + " INTEGER PRIMARY KEY," +
                        PharmEntry.PH_COL_NAME + " TEXT," +
                        PharmEntry.PH_COL_TIMELAPSE + " INTEGER," +
                        PharmEntry.PH_COL_DATEDONE + " INTEGER," +
                        PharmEntry.PH_COL_THID + " INTEGER)";

        public static final String SQL_DELETE_PHARMS =
                "DROP TABLE IF EXISTS " + PharmEntry.PH_TABLE_NAME;
    }

