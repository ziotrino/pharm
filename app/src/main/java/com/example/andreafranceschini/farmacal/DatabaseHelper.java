package com.example.andreafranceschini.farmacal;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.io.Console;

/**
 * Created by andreafranceschini on 06/04/17.
 */

public class DatabaseHelper extends SQLiteOpenHelper {
    public static final int DATABASE_VERSION = 2;
    public static final String DATABASE_NAME = "PharmCal.db";

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        /* dummy call to create database, remove after first run */
        SQLiteDatabase db = this.getWritableDatabase();
        Log.println(Log.INFO ,"db",db.getPath() );
        Log.println(Log.INFO ,"db",DataContract.SQL_CREATE_THREADS );
        Log.println(Log.INFO ,"db",DataContract.SQL_CREATE_PHARMS );
    }

    public void onCreate(SQLiteDatabase db) {
        db.execSQL(DataContract.SQL_CREATE_THREADS);
        db.execSQL(DataContract.SQL_CREATE_PHARMS);
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // This database is only a cache for online data, so its upgrade policy is
        // to simply to discard the data and start over
        db.execSQL(DataContract.SQL_DELETE_PHARMS);
        db.execSQL(DataContract.SQL_DELETE_THREADS);
        onCreate(db);
    }
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }

}
