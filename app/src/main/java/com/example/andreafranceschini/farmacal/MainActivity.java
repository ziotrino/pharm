package com.example.andreafranceschini.farmacal;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //test creazione db
        DatabaseHelper db = new DatabaseHelper(this);
        SQLiteDatabase data = db.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(DataContract.ThreadEntry.TH_COL_NAME, "prova");
        values.put(DataContract.ThreadEntry.TH_COL_ALERT, 1);
        long newRowId = data.insert(DataContract.ThreadEntry.TH_TABLE_NAME, null, values);
        Log.println(Log.INFO,"recID",Long.toString( newRowId));

        data.close();
    }
}
